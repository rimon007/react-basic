import React from 'react';
import { render } from 'react-dom';

import { Header } from './components/layout/Header';
import { Home } from './components/Home';
 
class App extends React.Component {
	constructor() {
		super();

		this.state = {
			linkName: 'Home'
		};
	}

	onChangeHeaderLink(newLink) {
		this.setState({
			linkName: newLink
		});
	}

	onGreet() {
		alert('Say! Hello.');
	}

	render() {
		let user = {
			name: 'It"s cool.',
			age: 25,
			hobbies: ['sports', 'programming', 'eating']
		};

		return (
			<div className="container">
				<Header linkName={this.state.linkName}/> 
				
				<Home 
					name={'Rimon'} 
					age={55} 
					user={user} 
					onGreet={this.onGreet}
					onChangeHeaderLink={this.onChangeHeaderLink.bind(this)}
					initialLinkName={this.state.linkName}
				/>			
			</div>
		);
	}
}

render(<App/>, window.document.getElementById('app'));