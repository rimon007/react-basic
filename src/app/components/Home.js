import React from 'react';
import PropTypes from 'prop-types';

export class Home extends React.Component {
	constructor(props) {
		super();

		this.state = {
			age: props.age,
			status: 0,
			linkName: props.initialLinkName
		};
	}

	onMakeOlder() {
		this.setState({
			age: this.state.age + 4
		});
	}

	onChangeLinkHeader() {
		this.props.onChangeHeaderLink(this.state.linkName);
	}

	onHandleChange(event) {
		this.setState({
			linkName: event.target.value
		});
	}

	render() {
		return(
			<div className="container">
				<div className="jumbotron">
			        <h1>Navbar example</h1>
			        <p>This example is a quick exercise to illustrate how the default, static navbar and fixed to top navbar work. It includes the responsive CSS and HTML, so it also adapts to your viewport and device.</p>
			        <p>
			          <a className="btn btn-lg btn-primary" href="../../components/#navbar" role="button">View navbar docs &raquo;</a>
			        </p>
		     	</div>

				<div className="row">
					<div className="col-md-12">
						<div className="panel panel-primary">
						  <div className="panel-body">
						    Panel content
						  </div>
						  <div className="panel-footer">
						  		<p>
						  			Your name is {this.props.name}, Your age is {this.state.age}
						  		</p>
						  		<p>
						  			Status: {this.state.status}
						  		</p>
						  		<button onClick={this.onMakeOlder.bind(this)} type="button" className="btn btn-primary">+ Age</button>
						  		
						  		<hr/>
							    <ul>
							  		{ this.props.user.hobbies.map((hobby, i) => <li key={i}>{hobby}</li>) }
							    </ul>	
							    <hr/>

							    <button type="button" onClick={this.props.onGreet} className="btn btn-danger">Say Hello</button>
							    <hr/>
							    <input type="text" 
							    	value={this.state.linkName}
							    	onChange={(event) => this.onHandleChange(event)} />
							    <button type="button" onClick={this.onChangeLinkHeader.bind(this)} className="btn btn-info">Change Link</button>

						  </div>
						</div>				
					</div>
				</div>
			</div>
		);
	}
}

Home.propTypes = {
	user: PropTypes.object,
	onGreet: PropTypes.func
}